/*
 * GNOME Pedia
 * Copyright (C) 2004 Hendrik Richter <hendrik@gnome-de.org>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

namespace GnomePedia
{
	using System;
	using System.Net;
	using System.Collections;
	using System.IO;
	using System.Text;
	using System.Text.RegularExpressions;
	using Gtk;
	using Glade;
	using Gecko;
	using Gnome;
	using GConf;
	using Mono.Posix;
	
	public class GladeApp
	{
		[Widget] Frame frameWeb;
		[Widget] Gtk.Entry entryKeyword;
		[Widget] Gtk.Window mainWindow;
		[Widget] Gtk.Button btnGo;
		[Widget] Gtk.Button btnBack;
		[Widget] Gtk.Button btnForward;
		[Widget] Gnome.AppBar appbar;
	
		private Gecko.WebControl web;
		
		private ArrayList History = new ArrayList ();
		private int HistoryPosition = 0;
		
		private static string[] authors = {
			"Hendrik Richter <hendrik@gnome-de.org>"
		};

		public static void Main (string[] args)
		{
			Gnome.Program program = new Gnome.Program ("GNOMEPedia", "0.1", Gnome.Modules.UI, args);
			new GladeApp (args);
		}
	
		public GladeApp (string[] args)
		{
			Glade.XML gxml = new Glade.XML (null, "gui.glade", "mainWindow", null);
			gxml.Autoconnect (this);

			mainWindow.Resize (
				(Settings.MainWindowWidth > 0) ? (int)Settings.MainWindowWidth : 700, 
				(Settings.MainWindowHeight > 0) ? (int)Settings.MainWindowHeight : 600
			);
			entryKeyword.GrabFocus ();
			// FIXME: focus entryKeyword
								
			web = new WebControl();
			web.OpenUri += LinkClicked;
			web.LinkMsg += LinkMessage;
			
			frameWeb.Add(web);
			web.RenderData ("<h1>Welcome to GNOME Pedia</h1>", "file:///tmp", "text/html");
			web.Show ();
			
			mainWindow.ShowAll ();

			Gtk.Application.Run();
		}

		public void DisplayWikiPage (string keyword, bool AddToHistory)
		{
			string url = "";
			
			// FIXME: use asynchronous threads
			// FIXME: make use of progress bar
			
			// transform keyword "foo" to wiki link to "foo" 
			if (!keyword.StartsWith (Settings.WikipediaUrl)) {
				url = Settings.WikipediaUrl+keyword;
			} else {
				url = keyword;
			}
			
			if (AddToHistory == true) {
				// if we can go forward but click on a link we have to delete
				// the history after the current position
				if (HistoryPosition < History.Count) {
					History.RemoveRange (HistoryPosition, History.Count - HistoryPosition);
				}
				History.Add (url);
			}

			// FIXME: ugly hack; check if it works for every language
			string html = LoadHtml(url);
			string expression = @"(?<Header>.*)<body(.*)<h1(.*)>(?<Title>.*)</h1>(.*)<!-- start content -->(?<Content>.*)<!-- end content -->";
 			Match ContentMatch = Regex.Match(html, expression);
			html = ContentMatch.Groups["Header"].Value+"<body class=\"ns-0\"><div id=\"content\" style=\"background-color:#ffffff;margin:1em 1em 1em 1em; border-right:1px solid #aaaaaa;\"><h1 class=\"firstHeading\">"+ContentMatch.Groups["Title"].Value+"</h1><div id=\"bodyContent\">"+ContentMatch.Groups["Content"].Value+"</div></body></html>";
			web.RenderData (html, Settings.WikipediaUrl, "text/html");
			
			UpdateButtonStatus ();
			entryKeyword.Text = ContentMatch.Groups["Title"].Value;

			HistoryPosition++;
		}

		public static string LoadHtml (string url)
		{
			// FIXME: what about using proxies?
			string content = "";

			try {
				WebRequest request = WebRequest.Create (url);
				if (request != null) {
					WebResponse response = request.GetResponse ();
					using (StreamReader sr = new StreamReader (response.GetResponseStream ())) {
						String line;
						while ((line = sr.ReadLine ()) != null) {
							content += line;
						}
					}
				}
			} catch (Exception e) { 
				System.Console.WriteLine("(LoadHtml) Couldn't load URL: " + url);
			}
			return content;
		}

		public void UpdateButtonStatus ()
		{
			btnBack.Sensitive = HistoryPosition > 0;
			btnForward.Sensitive = HistoryPosition+1 < History.Count;
		}

		public void Quit ()
		{
			// save main window's size to gconf
			int width = 0;
			int height = 0;
			mainWindow.GetSize(out width, out height);

			Settings.MainWindowWidth = width;
			Settings.MainWindowHeight = height;

			Application.Quit ();
		}


		//
		// functions dealing with gecko signals
		//
		public void LinkClicked (object o, Gecko.OpenUriArgs args)
		{
			// check if Uri links to wikipedia
			if (args.AURI.StartsWith (Settings.WikipediaUrl)) {
				DisplayWikiPage (args.AURI, true);
			} else {
				if (Settings.OpenExternalLinks) {
					// web.LoadUrl (args.AURI);
					// FIXME: we can't use this as it causes the signal Gecko.OpenUri
					//        which is linked to this function.
				} else {
					try {
						Gnome.Url.Show(args.AURI);
					} catch (Exception e) {
				                Console.WriteLine("(LinkClicked) Couldn't load URL: " + args.AURI);
					}
				}
			}
			
			// do not open the link directly
			args.RetVal = 1;
		}
		
		public void LinkMessage (object o, EventArgs args)
		{
			appbar.Push (web.LinkMessage.Replace (Settings.WikipediaUrl, ""));
		} 


		//
		// functions dealing with glade signals
		//
		public void OnWindowDeleteEvent (object o, DeleteEventArgs args)
		{
			Quit ();
		}		
		
		void btnBackClicked (object o, EventArgs args)
		{
			HistoryPosition -= 2;
			DisplayWikiPage (History[(int)HistoryPosition].ToString (), false);
		}
	
		void btnForwardClicked (object o, EventArgs args)
		{
			DisplayWikiPage (History[(int)HistoryPosition].ToString (), false);
		}
		
		void entryKeywordChanged (object o, EventArgs args)
		{
			btnGo.Sensitive = !entryKeyword.Text.Trim().Equals("");
		}

		void btnGoClicked (object o, EventArgs args)
		{
			DisplayWikiPage (entryKeyword.Text.Trim(), true);
		}

		void entryKeywordActivated (object o, EventArgs args)
		{
			DisplayWikiPage (entryKeyword.Text.Trim(), true);
		}
	
		void menuQuit (object o, EventArgs args)
		{
			Quit ();
		}
		
		void menuPreferences (object o, EventArgs args)
		{
			Preferences.Show(mainWindow);
		}
		
		void menuAbout (object o, EventArgs args)
		{
			new Gnome.About (
				"GNOME Pedia", "0.1-CVS",
				"(C) 2004 Hendrik Richter",
				"A simple application for browsing the Wikipedia",
				authors,
				null,
				null,
				null
			).Show ();
		}
	}	
}
