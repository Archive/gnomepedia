/*
 * GNOME Pedia
 * Copyright (C) 2004 Hendrik Richter <hendrik@gnome-de.org>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

namespace GnomePedia
{
	public class Settings
	{
		static GConf.Client client = new GConf.Client ();

		public static event GConf.NotifyEventHandler Changed
		{
			add {
				client.AddNotify ("/apps/gnomepedia", value);
			}
			remove{
				client.RemoveNotify ("/apps/gnomepedia", value);
			}
		}

		public static string WikipediaUrl
		{
			get {
				try {
					return (string) client.Get ("/apps/gnomepedia/wikipedia_url");
				} catch {
					return "http://en.wikipedia.org/wiki/";
				}
			}
			set {
				client.Set ("/apps/gnomepedia/wikipedia_url", value);
			}
		}

		public static event GConf.NotifyEventHandler WikipediaUrlChanged
		{
			add {
				client.AddNotify ("/apps/gnomepedia/wikipedia_url", value);
			}
			remove{
				client.RemoveNotify ("/apps/gnomepedia/wikipedia_url", value);
			}
		}

		public static int MainWindowWidth
		{
			get {
				try {
					return (int) client.Get ("/apps/gnomepedia/main_window_width");
				} catch {
					return 700;
				}
			}
			set {
				client.Set ("/apps/gnomepedia/main_window_width", value);
			}
		}

		public static event GConf.NotifyEventHandler MainWindowWidthChanged
		{
			add {
				client.AddNotify ("/apps/gnomepedia/main_window_width", value);
			}
			remove {
				client.RemoveNotify ("/apps/gnomepedia/main_window_width", value);
			}
		}

		public static int MainWindowHeight
		{
			get {
				try {
					return (int) client.Get ("/apps/gnomepedia/main_window_height");
				} catch {
					return 600;
				}
			}
			set {
				client.Set ("/apps/gnomepedia/main_window_height", value);
			}
		}

		public static event GConf.NotifyEventHandler MainWindowHeightChanged
		{
			add {
				client.AddNotify ("/apps/gnomepedia/main_window_height", value);
			}
			remove {
				client.RemoveNotify ("/apps/gnomepedia/main_window_height", value);
			}
		}

		public static bool OpenExternalLinks
		{
			get {
				try {
					return (bool) client.Get ("/apps/gnomepedia/open_external_links");
				} catch {
					return false;
				}
			}
			set {
				client.Set ("/apps/gnomepedia/open_external_links", value);
			}
		}

		public static event GConf.NotifyEventHandler OpenExternalLinksChanged
		{
			add {
				client.AddNotify ("/apps/gnomepedia/open_external_links", value);
			}
			remove {
				client.RemoveNotify ("/apps/gnomepedia/open_external_links", value);
			}
		}
	}

	public class SettingKeys
	{
		public static string WikipediaUrl
		{
			get {
				 return "/apps/gnomepedia/wikipedia_url";
			}
		}
		
		public static string MainWindowWidth
		{
			get {
				 return "/apps/gnomepedia/main_window_width";
			}
		}
		
		public static string MainWindowHeight
		{
			get {
				 return "/apps/gnomepedia/main_window_height";
			}
		}

		public static string OpenExternalLinks
		{
			get {
				return "/apps/gnomepedia/open_external_links";
			}
		}
	}
}
