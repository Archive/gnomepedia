/*
 * GNOME Pedia
 * Copyright (C) 2004 Hendrik Richter <hendrik@gnome-de.org>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

namespace GnomePedia
{
	using System;
	using Gtk;
	using Glade;
	using Gnome;
	using Mono.Posix;

	class Preferences
	{
		[Widget] Gtk.Window preferences;
		[Widget] Gtk.Entry entryUrl;
		[Widget] Gtk.CheckButton cbOpenExternalLinks;
	
		static Preferences preferencesBox;
		Gtk.Window parent;
	
		Preferences (Gtk.Window parent) {
			Glade.XML gladeXML = Glade.XML.FromAssembly ("gui.glade", "preferences", null);
			GConf.PropertyEditors.EditorShell shell = new GConf.PropertyEditors.EditorShell (gladeXML);

			shell.Add (SettingKeys.WikipediaUrl, "entryUrl");
			shell.Add (SettingKeys.OpenExternalLinks, "cbOpenExternalLinks");
			
			gladeXML.Autoconnect(this);
			this.parent = parent;
		}
	
		static public void Show (Gtk.Window parent)
		{
			if (preferencesBox == null) {
				preferencesBox = new Preferences (parent);
			}
			preferencesBox.preferences.Show ();
			// FIXME: set focus
		}
	
	       	void btnCloseClicked (object o, EventArgs args)
    		{
           		preferencesBox.preferences.Hide();
	       	}
	}
}
