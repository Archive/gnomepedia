#!/bin/sh
echo "Compiling..."
make -f Makefile.solution.GnomePedia "$@" && \
echo "Installing gconf scheme...the following warnings can be ignored..." ; \
gconftool-2 --install-schema-file=./gnomepedia.schema > /dev/null

